""" 
Programmieren Sie ein einfaches Quizspiel, das Fragen an den Nutzer stellt und die Eingaben (Antworten) des Nutzers entsprechend auswertet.

Sie können dabei selbst entscheiden: 
Werden dem Nutzer mehrer Antworten zu Wahl angeboten oder muss er die Antwort korrekt eingeben?
Soll der Nutzer nach der Antwort direkt ein Feedback bekommen oder wird erst am Ende ausgewertet?

Eine Mögliche Erweiterunge ist die Implementation eines Punktesystems für jede richtige Antwort.

"""

punkte = 0

print("Willkommen zum Quiz!")
print("Bitte geben Sie Ihren Namen ein:")
name = input()
print("Hallo " + name + "!")
print("Fragen werden nun gestellt. Bitte antworten Sie mit 1, 2 oder 3.")

print("Wie heißt die Hauptstadt von Deutschland?")
print("1. Berlin")
print("2. Hamburg")
print("3. München")
answere1 = input()
if answere1 == "1":
    print("Richtig!")
    punkte += 1
if answere1 == "2":
    print("Falsch!")
if answere1 == "3":
    print("Falsch!")

print("Wie heißt die Hauptstadt von Frankreich?")
print("1. Paris")
print("2. Marseille")
print("3. Lyon")
answere2 = input()
if answere2 == "1":
    print("Richtig!")
    punkte += 1
if answere2 == "2":
    print("Falsch!")
if answere2 == "3":
    print("Falsch!")

print("Wie heißt die Hauptstadt von Italien?")
print("1. Napoli")
print("2. Milano")
print("3. Rome")
answere3 = input()
if answere3 == "1":
    print("Falsch!")
if answere3 == "2":
    print("Falsch!")
if answere3 == "3":
    print("Richtig!")
    punkte += 1

print("Sie haben " + str(punkte) + " von 3 Punkten erreicht.")
print("Herzlichen Glückwunsch!")