""" 
Programmieren Sie einen einfachen Würfelsimulator, der immer wenn er Nutzer das Programm ausführt eine zufällige Zahl von 1-6 in der Konsole anzeigt.

Erweiterung:
Passe dein Programm so an, dass der Benutzer mehrere Würfe nacheinander durchführen kann und den Zeitpunkt zum Beenden des Programms selber bestimmt.
Außerdem soll es möglich sein die Anzahl der Würfelseiten anzupassen.

Bspl:
- 4-seitig mit Ziffen 1-4
- 6-seitig mit Ziffern 1-6
- 8-seitig mit Ziffern 1-8
- 10-seitig mit Ziffern 1-10
- 12-seitig mit Ziffern 1-12
- 20-seitig mit Ziffern 1-20
Bonus: 1x 10-seitig mit Ziffern 10-100

Erweitern Sie das Programm um weitere sinvolle Features. 
"""

import random
import time

print("Willkommen zum Würfelsimulator!")

while True:
    print("\n\n\n\n\n\n\nBitte geben Sie die Anzahl der Würfelseiten ein:")
    wuerfeleite = input()
    try:
        wuerfeleite = int(wuerfeleite)
    except ValueError:
        print("Bitte geben Sie eine Zahl ein!")
        continue
    if wuerfeleite != 4 and wuerfeleite != 6 and wuerfeleite != 8 and wuerfeleite != 10 and wuerfeleite != 12 and wuerfeleite != 20:
        print("Bitte geben Sie eine gültige Zahl ein! (4, 6, 8, 10, 12, 20)")
    else:
        print("Bitte drücken Sie die Enter-Taste um die Würfel zu starten!")
        input()
        print("\n\nDie Würfel werden jetzt gewürfelt...")
        time.sleep(1)
        print("------------------------------------------------------")
        print(random.randint(1, wuerfeleite))
        print("------------------------------------------------------")
        print("Möchten Sie eine weitere Würfelzahl erhalten? (y/n)")
        antwort = input()
        if antwort == "y":
            continue
        else:
            print("Auf Wiedersehen!")
            break