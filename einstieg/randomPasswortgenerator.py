"""
Ein sicheres Passwort zu erstellen und sich daran zu erinnern, ist keine leichte Aufgabe.
Erstellen Sie ein Programm, das einige Wörter des Benutzers aufnimmt und dann aus diesen Wörtern ein zufälliges Passwort generiert.
Der Benutzer kann sich anschließend, mit Hilfe der Wörter die er als Eingabe eingegeben hat, an das Passwort erinnern.
"""
import random

print("--- Passwortgenerator ---")
print("Bitte geben Sie einige Wörter ein, die Sie als Passwort verwenden wollen:")

woerterList = []

for i in range(4):
    woerterList.append(input("Wort " + str(i + 1) + ": "))

random.shuffle(woerterList)
langesPasswort = "".join(random.sample(woerterList, 4))

# Zufall der Groß und Kleinschreibung generieren 
for i in range(len(langesPasswort)):
    if random.randint(0, 1) == 0:
        langesPasswort = langesPasswort[:i] + langesPasswort[i].upper() + langesPasswort[i + 1:]
    else:
        langesPasswort = langesPasswort[:i] + langesPasswort[i].lower() + langesPasswort[i + 1:]

print("Das Passwort lautet: " + langesPasswort)
