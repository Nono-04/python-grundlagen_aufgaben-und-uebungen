"""
In einem Text sollen alle Zahlen addiert werden. 
Wenn eine Zahl Teil eines Wortes ist, wird sie nicht gezählt.
"""


# sum_numbers: Input: ein string (Zeichenfolge) / Output: ein int (eine Ganzzahl)
def sum_numbers(text: str) -> int: 

    # TODO: Hier ist der Platz für deinen Code
    
    return 0


if __name__ == '__main__':
    print("Example:")
    print(sum_numbers('hi'))

    # diese "asserts" dienen der Überprüfung des Codes 
    assert sum_numbers('hi') == 0
    assert sum_numbers('who is 1st here') == 0
    assert sum_numbers('my numbers is 2') == 2
    assert sum_numbers('5 plus 6 is') == 11
    assert sum_numbers('This picture is an oil on canvas '
                        'painting by Danish artist Anna '
                        'Petersen between 1845 and 1910 year') == 3755
    assert sum_numbers('') == 0
    print("Glückwunsch, alle Tests wurden bestanden.")
