# Ein Wort soll dahingehend überprüft werden, ob es sich um ein Palindrom handelt oder nicht. 

# palindromcheck: Input: ein string (Zeichenfolge) / Output: ein boolean (Wahrheitswert)
def palindromcheck(word: str) -> bool:

    # 1. Schritt: Wort in eine Liste aufteilen
    word_list = list(word.lower())
    # 2. Schritt: Liste umkehren
    word_list_reverse = word_list[::-1]

    # 3. Schritt: Wort in einen String zusammenfügen
    word_reverse = ''.join(word_list_reverse)

    # 4. Schritt: Wort und umgekehrtes Wort vergleichen
    if word.lower() == word_reverse:
        return True
    else:
        return False

if __name__ == '__main__':
    assert palindromcheck('Hallo') == False
    assert palindromcheck('neben') == True
    assert palindromcheck('REgalLaGer') == True
    assert palindromcheck('Hannah') == True
    assert palindromcheck('Rababer') == False
    print("Glückwunsch, alle Tests wurden bestanden.")
    
    