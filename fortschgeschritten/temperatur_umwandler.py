""" 
Schreiben Sie ein Programm, das Temperaturen in verschiedene Skalensysteme umwandelt. 
Das Programm soll zu Beginn dem Nutzer eine Auswahl mit den verschiedenen Möglichkeiten anbieten.

Bsp Auswahl: 
(1) Umrechnung von Celsius nach Kelvin
(2) Umrechnung von Celsius nach Fahrenheit
(3) Umrechnung von Kelvin nach Celsius
(4) Umrechnung von Kelvin nach Fahrenheit
(5) Umrechnung von Fahrenheit nach Celsius
(6) Umrechnung von Fahrenheit nach Kelvin

Eine kleine Hilfe:
Celsius = 5/9 * (Fahrenheit - 32).
Celsius = Kelvin - 273.15.
Die tiefste mögliche Temperatur ist der absolute Nullpunkt. => 0K
"""

print("(1) Umrechnung von Celsius nach Kelvin")
print("(2) Umrechnung von Celsius nach Fahrenheit")
print("(3) Umrechnung von Kelvin nach Celsius")
print("(4) Umrechnung von Kelvin nach Fahrenheit")
print("(5) Umrechnung von Fahrenheit nach Celsius")
print("(6) Umrechnung von Fahrenheit nach Kelvin")

choose = input()
number = input("Was möchtest du Umrechnen: ")

if choose == "1":
    # Umrechnung von Celsius nach Kelvin
    calc: int = float(number) + 273.15
    print("Kelvin: " + str(calc))
elif choose == "2":
    # Umrechnung von Celsius nach Fahrenheit
    calc: int = float(number) * 1.8 + 32
    print("Fahrenheit: " + str(calc))
elif choose == "3":
    # Umrechnung von Kelvin nach Celsius
    calc: int = float(number) - 273.15
    print("Celsius: " + str(calc))
elif choose == "4":
    # Umrechnung von Kelvin nach Fahrenheit
    calc: int = (float(number) - 273.15) * 1.8 + 32
    print("Fahrenheit: " + str(calc))
elif choose == "5":
    # Umrechnung von Fahrenheit nach Celsius
    calc: int = (float(number) - 32) / 1.8
    print("Celsius: " + str(calc))
elif choose == "6":
    # Umrechnung von Fahrenheit nach Kelvin
    calc: int = (float(number) - 32) / 1.8 + 273.15
    print("Kelvin: " + str(calc))