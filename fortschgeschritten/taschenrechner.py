""" 
Schreiben Sie ein Programm, das zwei Zahlen den Vorgaben entsprechend verrechnet. 
Das Programm soll zu Beginn dem Nutzer eine Auswahl mit den verschiedenen Möglichkeiten anbieten.

Bsp. Auswahl: 
(1) Addition
(2) Subtraktion
(3) Multiplikation
(4) Division

Im Anschluss:
Erweitere deine Lösung, indem du für die Berechnung 4 verschiedene Funktionen definierst. 
Passe ausserdem dein Programm so an, dass der Benutzer mehrere Berechnungen nacheinander durchführen kann und den Zeitpunkt zum Beenden des Programms selber bestimmt.
"""

while True:
    print("(1) Addition")
    print("(2) Subtraktion")
    print("(3) Multiplikation")
    print("(4) Division")

    input_user = input("Wähle deine Entscheidung: ")
    
    if input_user == "1":
        print("Addition")
        input_user_1 = int(input("Erste Zahl: "))
        input_user_2 = int(input("Zweite Zahl: "))
        print(input_user_1 + input_user_2)
    elif input_user == "2":
        print("Subtraktion")
        input_user_1 = int(input("Erste Zahl: "))
        input_user_2 = int(input("Zweite Zahl: "))
        print(input_user_1 - input_user_2)
    elif input_user == "3":
        print("Multiplikation")
        input_user_1 = int(input("Erste Zahl: "))
        input_user_2 = int(input("Zweite Zahl: "))
        print(input_user_1 * input_user_2)
    elif input_user == "4":
        print("Division")
        input_user_1 = int(input("Erste Zahl: "))
        input_user_2 = int(input("Zweite Zahl: "))
        print(input_user_1 / input_user_2)
    else:
        print("Bitte nur die Zahlen 1, 2, 3 oder 4 eingeben!")
    print("Möchtest du noch eine weitere Zahl hintereinander berechnen? (y/n)")
    input_user_3 = input()
    if input_user_3 == "y":
        continue
    else:
        break
