"""
In diesem Projekt programmieren Sie ein rudimentäres Abenteuerspiel welches vollständig textbasiert ist. 
Ihr Spielt gibt das Setting und die Beschreibung der Räume vor. Der Nutzer kann sich mittels verschiedener Wahlmöglichkeiten in ihrem Setting entsprechend bewegen und handeln. Auf Grundlage der Entscheidungen des Nutzers ergeben sich weitere Aktionen oder Reaktionen in ihrem Setting.

Mögliche Erweiterungen:
- Sammlung und Nutung Inventargegenständen
- Schwierigkeitsgrade
- Speichermöglichkeiten
- mehrere Enden
- etc.
"""