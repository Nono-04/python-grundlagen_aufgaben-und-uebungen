"""
Kein Mitspieler vorhanden? Dann programmieren Sie einfach ihren eigenen Spielpartner.

Zuerst trifft der nutzer eine Entscheidung (Schere, Stein oder Papier) und dann trifft der Computer seine. Damit der Nutzer seinen Zug machen kann, können Sie entweder die Enscheidungen durch Auswahlbuchstaben vorgeben oder den Nutzer das Wort (Schere, Stein oder Papier) eintippen lassen.

Am Ende wird auf Basis der beiden Entscheidungen (Nutzer, Computer) ein Gewinner der Runde ermittelt.
Sie können dann entweder die Möglichkeit geben erneut zu spielen oder den Nutzer im Voraus eine Anzahl von Runden festgelegte lassen. Es muss natürlich eine Funktion zur Punkteverfolgung eingerichtet werden, die am Ende den Sieger ermittelt.
"""

while True:
    print("Schere, Stein oder Papier?")
    print("Schere: 1")
    print("Stein: 2")
    print("Papier: 3")

    input_user = input("Wähle deine Entscheidung: ")

    import random
    random_number = random.randint(1, 3)

    if input_user == "1":
        if random_number == 1:
            print("Du hast Schere gewählt und der Computer hat Schere gewählt. Unentschieden!")
        elif random_number == 2:
            print("Du hast Schere gewählt und der Computer hat Stein gewählt. Du hast verloren!")
        elif random_number == 3:
            print("Du hast Schere gewählt und der Computer hat Papier gewählt. Du hast gewonnen!")
    elif input_user == "2":
        if random_number == 1:
            print("Du hast Stein gewählt und der Computer hat Schere gewählt. Du hast gewonnen!")
        elif random_number == 2:
            print("Du hast Stein gewählt und der Computer hat Stein gewählt. Unentschieden!")
        elif random_number == 3:
            print("Du hast Stein gewählt und der Computer hat Papier gewählt. Du hast verloren!")
    elif input_user == "3":
        if random_number == 1:
            print("Du hast Papier gewählt und der Computer hat Schere gewählt. Du hast verloren!")
        elif random_number == 2:
            print("Du hast Papier gewählt und der Computer hat Stein gewählt. Du hast gewonnen!")
        elif random_number == 3:
            print("Du hast Papier gewählt und der Computer hat Papier gewählt. Unentschieden!")
    else:
        print("Bitte nur die Zahlen 1, 2 oder 3 eingeben!")
    print("Möchtest du noch einmal spielen? (j/n)")
    input_user_2 = input("Wähle deine Entscheidung: ")
    if input_user_2 == "j":
        continue
    else:
        print("Danke für's Spielen!")
        break
